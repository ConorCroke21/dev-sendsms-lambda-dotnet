using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using dev_sendsms_lambda_dotnet;
using System.ComponentModel.DataAnnotations;

namespace dev_sendsms_lambda_dotnet.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToUpperFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            SendReports test = new SendReports();
            test.Request = new Request();
            test.Request.Email = new Properties();
            test.Request.Email.enabled = true;

            var lambdaInvoke = function.FunctionHandler(test, context);

            Assert.Equal(test, lambdaInvoke);
        }
    }
}
