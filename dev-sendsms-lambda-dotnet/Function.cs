using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace dev_sendsms_lambda_dotnet
{
    public class Function
    {
        public SendReports FunctionHandler(SendReports sms, ILambdaContext context)
        {
            //var messageId = SendSMS(sms);

            ////Assign variables to response Json
            //var response = new Sms();
            //response.smsId = messageId;
            //response.To = sms.Request.Sms.To;
            //response.Body = sms.Request.Sms.Body;

            
            return sms;
        }

        private string SendSMS(SendReports sms)
        {
            const string accountSid = "Test";
            const string authToken = "Test";

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                to: new Twilio.Types.PhoneNumber(sms.Request.SMS.to),
                from: new Twilio.Types.PhoneNumber(sms.Request.SMS.from),
                body: sms.Request.SMS.body);

            return message.Sid;
        }
    }
}
