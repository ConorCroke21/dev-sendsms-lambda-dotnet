﻿using Newtonsoft.Json;

namespace dev_sendsms_lambda_dotnet
{
    public class SendReports
    {
        [JsonProperty("Request")]
        public Request Request { get; set; }
    }

    public partial class Request
    {
        [JsonProperty("SMS")]
        public Properties SMS { get; set; }

        [JsonProperty("Email")]
        public Properties Email { get; set; }
    }

    public partial class Properties
    {
        [JsonProperty("enabled")]
        public bool enabled { get; set; }

        [JsonProperty("to")]
        public string to { get; set; }

        [JsonProperty("from")]
        public string from { get; set; }

        [JsonProperty("subject")]
        public string subject { get; set; }

        [JsonProperty("body")]
        public string body { get; set; }
    }
}